import React from "react";
import "./App.css";
import logo from "./logo.svg";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>gitlab-ci.yaml</code> and see your pipeline
        </p>
        <a
          className="App-link"
          href="https://docs.gitlab.com/ee/ci/pipelines/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn Gitlab
        </a>
      </header>
    </div>
  );
}

export default App;
